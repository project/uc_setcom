

uc_Setcom is a Ubertcart payment gateway for Setcom.com

*Original development sponsored by Setcom*
*Drupal 6 upgrade sponsored by www.synecobusinessit.co.za*

Setcom is an easy, convenient and secure way to send or receive money on-line and on your mobile phone.
	
*For Buyers*

Use your Setcom account to pay for products and services purchased on the web or with your mobile phone. You can send payments to anyone with an email address in 200 countries and territories worldwide. It's free to send money, and works with your existing credit card, debit card, and bank account.

*For Sellers*

Use your Setcom account to accept payments on your website from customers worldwide, in multiple currencies. Cut down on the administrative hassles of handling fund transfers, so you can focus on your business. We take care of getting you paid. 

Visit Setcom for more information: http://www.setcom.com
Support development for this project by using this link to sign up for a merchant account: https://www.setcom.com/www/signup/?SRI=BCB2A357A9
(Sign up by referral. No cost to you.)

*Setcom has the following payment methods:*

    * Credit Card
    * Available balance at Setcom.
    * eCheque - Make payment by issuing a cheque. Has a waiting period for transfer of funds.
    * eDeposit - Make payment by deposit into Setcom account. Has a shorter waiting period.
    * POLi - Instant payment by Electronic Fund Transfer. see: www.poli.co.za/

*Features of this module:*

    * Selection of allowable payment methods.
    * Workflow-ng rules for payment type actions.
    * Complete error report on payment failures.

Developed by Quentin Campbell (Max_Headroom) www.maxheadroom.co.za 
Live testing and input from Setcom support (Elsb� Fourie).

*Installation:*
No special instructions to install. Upload and enable module as normal.

*Configure:*
In admin/store/settings/payment enable Setcom as a payment method. 
Under admin/store/settings/payment/edit/methods, click on Setcom settings.
Set Transaction mode to either Test or Production. Use Test to test your installation (See Testing instructions below.)
Enter your Setcom user name and password you registered with at Setcom. Then enter the Merchant ID supplied to you by Setcom.

Complete the configuration following the instructions on the form.

Please note that you can set how you want to allow the Setcom payment methods (Credit Card, POLi, Available credit, eCheque, eDeposit) to be used. You can select either to: allow immediate payment, ship only after payment received, or not allow payment method.
But Setcom is *not* informed about the setting. For instance, a buyer can still make a payment by eDeposit even if you do not allow this method. There are instructions for the buyer on the checkout page on which payments are allowed, and if a buyer still use a method at Setcom that is not allowed here, then on redirect from Setcom the buyer will be warned that this transaction can not be honoured, and that the buyer should contact Setcom to rectify this. An email will also be send to the store administrator (or the email you selected for error reporting). The order status is also set to "Setcom payment denied".

If you allow shipping after payment received, you will also not be informed by Setcom when payment is received. You have to check  your account manually for when payment is made and also update the status of the order manually (Order is marked "Setcom payment pending").

Allowed and successful transactions are marked "Setcom payment received" and order state is "Payment received".

*Testing instructions:*
You can have a fully functional test site by putting the Transaction mode on Test and set-up the test seller user name, test password and test merchant id as per instructions in Setcom settings.
You can use 5 test accounts, named from testseller1@setcom.com (password: testseller. Merchant ID: 1234567891) to testseller5@setcom.com (password: testseller. Merchant ID: 1234567895). Note the change in last digit in Merchant ID.

Please note that you must log into Setcom using the test seller name and password you've selected to use and under Profile >> Selling Preferences >> Website Payment Preferences, enter the Return URL (found in Setcom settings) into the Redirect URL field under Auto-Redirect.
As these are test accounts, other developers can also log in and change the return url for their needs. Select another test account until you find one that is not used. 
You can now use testbuyer@setcom.com, password: testbuyer, to make purchases from your site.
Currently, only credit card can be used for test payment.
